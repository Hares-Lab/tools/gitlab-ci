
"""
====================================
Markdown PrependNewLine Extension
====================================
:copyright: Copyright 2019 Ayobami Adewale
:copyright: Copyright 2020 Peter Zaitcev
:license: MIT, see LICENSE for details.

"""

from __future__ import absolute_import
from __future__ import unicode_literals

import re

from markdown.extensions import Extension
from markdown.preprocessors import Preprocessor


class PrependNewLineExtension(Extension):
    """ PrependNewLine Extension for Python-Markdown. """

    def extendMarkdown(self, md):
        """ Insert PrependNewLinePreprocessor before ReferencePreprocessor. """
        md.preprocessors.register(PrependNewLinePreprocessor(md), 'prependnewline', 12)


class PrependNewLinePreprocessor(Preprocessor):
    """ PrependNewLine Preprocessor - finds list in a document and prepends a newline. """

    def run(self, lines):
        """
        Find all list from the text.
        Each reference is prepended with a new line.
        """
        new_text = []
        skip_next: bool = True
        for line in lines: # type: str
            if (len(line.strip()) > 0):
                if (re.match(r'\s*(\d+\.|[*+-])\s+', line)):
                    if (not skip_next):
                        line = '\n' + line
                    elif (line[0].isspace()):
                        line = ((len(line) - len(line.lstrip())) // 2 + 1) * ' ' + line
                    skip_next = True
                elif (line[0].isspace()):
                    skip_next = True
                elif (line.lstrip()[0].startswith('#')):
                    skip_next = True
                else:
                    skip_next = False
            else:
                skip_next = True
            
            new_text.append(line)
        return new_text


def makeExtension(**kwargs):  # pragma: no cover
    return PrependNewLineExtension(**kwargs)
