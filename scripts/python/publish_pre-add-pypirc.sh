#!/usr/bin/env bash

set -eu
set +x

echo PYPI_REPOSITORY is "${PYPI_REPOSITORY:=}"
if [[ -z "${PYPI_REPOSITORY}" ]]
then REPO_PREFIX='# repository not set'
else REPO_PREFIX='repository'
fi

cat > ~/.pypirc << EOL
[distutils]
index-servers=
    pypi

[pypi]
${REPO_PREFIX} = ${PYPI_REPOSITORY}
username = ${PYPI_USER}
password = ${PYPI_PASSWORD}
EOL
