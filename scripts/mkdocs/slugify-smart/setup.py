from setuptools import setup

setup \
(
    name = 'slugify-smart',
    version = '1.0.0',
    author = 'Peter Zaitcev',
    author_email = 'ussx-hares@yandex.ru',
    description = 'Custom Slugify function for Markdown TOC',
    long_description_content_type = 'text/markdown',
    url = 'https://gitlab.com/Hares-Lab/gitlab-ci/scripts/mkdocs/slugify-smart',
    py_modules = [ 'slugify_smart' ],
    install_requires = [ ],
    classifiers =
    [
        'Operating System :: OS Independent',
        'License :: OSI Approved :: MIT License',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Topic :: Text Processing :: Filters',
        'Topic :: Text Processing :: Markup :: HTML'
    ]
)
