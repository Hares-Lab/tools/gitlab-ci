#!/usr/bin/env bash

set -eu

echo "Actual version: ${ACTUAL_VERSION}"
INDEX=${PYPI_REPOSITORY:-}
if [[ ! -z "${INDEX}" ]]
then INDEX="--index ${INDEX%/}/pypi"
else echo "PyPI repository not specified, omitting"
fi

if (pip search ${INDEX} --no-color "${PACKAGE_NAME}" | grep -F -- "${PACKAGE_NAME}" | grep -F -- "${ACTUAL_VERSION}")
then
    echo "Package version ${ACTUAL_VERSION} already deployed" >&2
    exit 1
fi
