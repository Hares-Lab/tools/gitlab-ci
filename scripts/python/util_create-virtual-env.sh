#!/usr/bin/env bash

set -eu

python --version
pip install virtualenv

virtualenv "${VIRTUALENV_NAME}" ${VIRTUALENV_ARGS-}
