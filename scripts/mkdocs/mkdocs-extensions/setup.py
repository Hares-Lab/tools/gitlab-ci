from setuptools import setup

setup \
(
    name = 'mkdocs-extensions',
    version = '1.1.1',
    author = 'Peter Zaitcev',
    author_email = 'ussx-hares@yandex.ru',
    description = 'MkDocs extension plugins',
    long_description_content_type = 'text/markdown',
    url = 'https://gitlab.com/Hares-Lab/gitlab-ci/scripts/mkdocs/mkdocs-extensions',
    py_modules = [ 'mkdocs_extensions' ],
    install_requires = [ 'mkdocs>=1.1' ],
    entry_points =
    {
        'mkdocs.plugins':
        [
            'root = mkdocs_extensions:RootPlugin',
            'readme = mkdocs_extensions:ReadMePlugin',
            'index = mkdocs_extensions:IndexPlugin',
            'wildcard-css = mkdocs_extensions:WildcardCSSPlugin',
        ]
    },
    classifiers =
    [
        'Operating System :: OS Independent',
        'License :: OSI Approved :: BSD License',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Topic :: Text Processing :: Filters',
        'Topic :: Text Processing :: Markup :: HTML'
    ]
)
