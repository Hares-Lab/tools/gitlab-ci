import os
import re
import shutil
from glob import iglob
from logging import getLogger
from tempfile import mkstemp
from typing import *
from urllib.parse import urlparse

from typing.io import *

try:
    from hashlib import file_digest
except ImportError:
    import hashlib
    if (TYPE_CHECKING):
        from _hashlib import HASH as Hash
    # else:
    #     Hash: Type = cast(type, None)
    
    def file_digest(fileobj: BinaryIO, digest: Literal[hashlib.algorithms_guaranteed], /):
        constructor = getattr(hashlib, digest) # type: Type[Hash]
        file_hash = constructor()
        while chunk := fileobj.read(8192):
            file_hash.update(chunk)
        return file_hash

def file_hash(file: str) -> str:
    with open(file, 'rb') as f:
        return file_digest(f, 'sha256').hexdigest()

try:
    from mkdocs.livereload import LiveReloadServer as Server
except ImportError:
    Server = None
from mkdocs.config import config_options
from mkdocs.config.defaults import MkDocsConfig
from mkdocs.plugins import BasePlugin
from mkdocs.structure.files import File
from mkdocs.structure.pages import Page

_AUTO = '<auto>'
_DEFAULT_ENCODING = 'utf-8-sig'

class RootPlugin(BasePlugin):
    logger = getLogger('mkdocs.plugins.root')
    
    config_scheme = \
    (
        ('absolute', config_options.Type(bool, default=False)),
        ('root_url', config_options.Type(str, default=_AUTO)),
        ('encoding', config_options.Type(str, default=_DEFAULT_ENCODING)),
    )
    
    def on_config(self, config, **kwargs):
        root_url = self.config['root_url']
        if (root_url == _AUTO):
            root_url = config['site_url']
            if (not self.config['absolute']):
                root_url = urlparse(root_url).path
            root_url = root_url.rstrip('/')
            self.config['root_url'] = root_url
        
        return config
    
    def on_post_build(self, config, **kwargs):
        for css in config['extra_css']:
            dest = os.path.join(config['site_dir'], css)
            with open(dest, 'rt', encoding=self.config['encoding'], errors='strict') as f:
                content = f.read()
            
            processed = self.process_file_content(content)
            if (processed is not None):
                self.logger.debug(f"Updating media file: '{dest}'")
                with open(dest, 'wt', encoding=self.config['encoding']) as f:
                    f.write(processed)
    
    def on_page_read_source(self, page: Page, config, **kwargs):
        pf: File = page.file
        with open(pf.abs_src_path, 'rt', encoding=self.config['encoding'], errors='strict') as f:
            source = f.read()
        
        result = self.process_file_content(source)
        if (result is not None):
            self.logger.debug(f"Pre-processing file '{pf.name}'")
        return result
    
    def process_file_content(self, content: str) -> Optional[str]:
        processed = re.sub(r'{%\s*root\s*%}', self.config['root_url'], content)
        if (processed != content):
            return processed
        else:
            return None

class ReadMePlugin(BasePlugin):
    """
    Plugin that places repository ReadMe as the root index page
    """
    logger = getLogger('mkdocs.plugins.readme')
    
    config_scheme = \
    (
        ('source',   config_options.Type(str, default='README.md')),
        ('docs_dir', config_options.Type(str, default='docs')),
        ('dest',     config_options.Type(str, default='index.md')),
        ('encoding', config_options.Type(str, default=_DEFAULT_ENCODING)),
    )
    
    def on_config(self, config: MkDocsConfig, **kwargs):
        self.config['dest'] = os.path.join(config['docs_dir'], self.config['dest'])
        return config
    
    def on_serve(self, server: Server, config: MkDocsConfig, builder, **kwargs):
        # dest_abs = os.path.abspath(self.config['dest'])
        server.watch(os.path.abspath(self.config['source']))
    
    def on_pre_build(self, config: MkDocsConfig, **kwargs):
        source: str = self.config['source']
        docs_dir: str = self.config['docs_dir']
        dest: str = self.config['dest']
        encoding: str = self.config['encoding']
        
        with open(source, 'rt', encoding=encoding) as f:
            content = f.read()
        
        content = re.sub(re.escape(docs_dir.rstrip('/') + '/'), '', content)
        _, temp = mkstemp(prefix='index.', suffix='.md')
        try:
            with open(temp, 'wt', encoding=encoding) as f:
                f.write(content)
            
            if (not os.path.exists(dest) or file_hash(temp) != file_hash(dest)):
                self.logger.debug(f"Copying ReadMe file: {source} -> {dest}")
                shutil.copy(temp, dest)
                os.chmod(dest, 0o644)
        
        finally:
            try:
                os.unlink(temp)
            except OSError:
                pass
        
        return None

class WildcardCSSPlugin(BasePlugin):
    logger = getLogger('mkdocs.plugins.wildcard-css')
    
    config_scheme = \
    (
        ('css_directory', config_options.Type(str, default='css/')),
        ('css_pattern', config_options.Type(str, default='*.css')),
    )
    
    def on_config(self, config, **kwargs):
        docs_dir = os.path.abspath(config['docs_dir'])
        extra_css: List[str] = config['extra_css']
        for css in iglob(os.path.join(docs_dir, self.config['css_directory'], self.config['css_pattern'])):
            rel_path = os.path.relpath(css, docs_dir).replace('\\', '/')
            if (not rel_path in extra_css):
                self.logger.debug(f"Found a new non-listed CSS file: '{rel_path}'")
                extra_css.append(rel_path)
        
        return config


__all__ = \
[
    'RootPlugin',
    'ReadMePlugin',
    'WildcardCSSPlugin',
]
