from markdown.inlinepatterns import InlineProcessor
from markdown.extensions import Extension
import xml.etree.ElementTree as etree

class InlineDiffInlineProcessor(InlineProcessor):
    def handleMatch(self, m, data):
        is_addition = m.group(1) == '+'
        
        el = etree.Element('span')
        classes = [ 'idiff', 'left', 'right' ]
        if (is_addition):
            classes.append('addition')
        else:
            classes.append('deletion')
        
        el.attrib['class'] = ' '.join(classes)
        el.text = m.group(2)
        return el, m.start(0), m.end(0)

class InlineDiffExtension(Extension):
    def extendMarkdown(self, md):
        pattern = r'{([+-])(.*?)\1}'  # like {+addition+} or {-deletion-}
        md.inlinePatterns.register(InlineDiffInlineProcessor(pattern, md), 'idiff', 175)

def makeExtension(**kwargs):
    return InlineDiffExtension(**kwargs)


__all__ = \
[
    'InlineDiffExtension',
    'InlineDiffInlineProcessor',
    'makeExtension',
]
