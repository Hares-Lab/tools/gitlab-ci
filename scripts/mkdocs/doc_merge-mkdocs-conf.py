#!/usr/bin/env python

"""
Works for both Python 2.7 and for Python 3.x
Merges YAML configuration from multiple sources
See 'usage'
"""

from __future__ import print_function

import os
import sys
from copy import deepcopy
from functools import partial
from glob import iglob
from logging import getLogger, basicConfig
from shutil import copy2

import yaml

try:
    from typing import *
    from typing.io import *
except ImportError:
    typing = None
    Any = None
    Dict = dict
    def TypeVar(name, *args, **kwargs): pass

try:
    ConfigType = Dict[str, Any]
except TypeError:
    ConfigType = Dict

CONFIG_PART_SUFFIX = '.yml'
CONFIG_DEFAULT_NAME = 'mkdocs.default.yml'
DEFAULT_KEY = '#default'

logger = getLogger('mkdocs.plugins.merge-configs')

def load_yaml(path):
    # type: (Union[str, BinaryIO]) -> ConfigType
    
    if (isinstance(path, str)):
        with open(path, 'rb') as io:
            return load_yaml(io)
    
    else:
        logger.debug("Reading file: {}".format(path))
        return yaml.unsafe_load(path)
        # return yaml.safe_load(path)

def write_yaml(path, config):
    # type: (Union[str, TextIO], ConfigType) -> None
    if (isinstance(path, str)):
        with open(path, 'wt', encoding='utf-8') as io:
            return write_yaml(io, config)
    
    else:
        logger.debug("Writing file: {}".format(path))
        return yaml.dump(config, path, encoding='utf-8')

def new_config():
    # type: () -> ConfigType
    return dict()

T = TypeVar('T')
def ordered_merge_sequence(a, b):
    # type: (Iterable[T], Iterable[T]) -> Iterator[T]
    
    a = list(a)
    b = list(b)
    
    intersection = set(a).intersection(b)
    current_iter = iter(a)
    other_iter = iter(b)
    while (current_iter is not None):
        try:
            x = next(current_iter)
        except StopIteration:
            current_iter, other_iter = other_iter, None
            continue
        
        if (not x in intersection):
            yield x
        else:
            intersection.remove(x)
            current_iter, other_iter = other_iter, current_iter
del T

def merge_yaml(old, new):
    # type: (Optional[ConfigType], ConfigType) -> ConfigType
    
    new = deepcopy(new)
    if (old is None):
        return new
    elif (new is None):
        new = dict()
    
    keys_sequence = ordered_merge_sequence(old.keys(), new.keys())
    old = deepcopy(old)
    
    for key, value in new.items():
        if (key in old and isinstance(value, dict)):
            value = merge_yaml(old[key], value)
        
        old[key] = value
    
    result = new_config()
    for key in keys_sequence:
        result[key] = old[key]
    
    return result

def apply_defaults(config, includes):
    # type: (ConfigType, Optional[ConfigType]) -> ConfigType
    
    config = deepcopy(config)
    if (includes is None):
        return config
    includes = deepcopy(includes)
    
    for key, value in includes.items():
        if (key not in config):
            continue
        elif (isinstance(value, dict)):
            new_value = apply_defaults(config[key], value)
        elif (isinstance(value, list) and isinstance(config[key], list) and DEFAULT_KEY in config[key]):
            old = config[key] # type: list
            id = old.index(DEFAULT_KEY)
            new_value = old[:id] + value + old[id + 1:]
        else:
            new_value = config[key]
        
        config[key] = new_value
    
    return config

def merge_configs(source_dir, includes, result):
    # type: (str, ConfigType, Dict[str, ConfigType]) -> Tuple[ConfigType, Dict[str, ConfigType]]
    
    p = os.path.join(source_dir, CONFIG_DEFAULT_NAME)
    if (os.path.isfile(p)):
        j = load_yaml(p)
        includes = merge_yaml(includes, j)
    
    for p in iglob(os.path.join(source_dir, '*' + CONFIG_PART_SUFFIX)):
        if (p.endswith(CONFIG_DEFAULT_NAME)):
            continue
        
        j = load_yaml(p)
        key = os.path.basename(p)
        old = result.get(key, None)
        old = merge_yaml(old, includes)
        result[key] = merge_yaml(old, j)
    
    return includes, result

def _merge_ignore_func(dest, path, files):
    # type: (str, str, Iterable[str]) -> Iterator[str]
    if (os.path.abspath(dest) != os.path.abspath(path)):
        for f in files: yield f
    
    else:
        for f in files:
            if (not f.endswith('.yml')):
                yield f
            elif (f.endswith(CONFIG_DEFAULT_NAME)):
                pass
            elif (f.endswith(CONFIG_PART_SUFFIX)):
                pass
            else:
                yield f

def copy_dir(source_dir, dest_dir, mode=0o755, ignore=None):
    # type: (str, str, int, Optional[Callable[[str, List[str]], Iterable[str]]]) -> None
    logger.debug("Merging directory: {} -> {}".format(source_dir, dest_dir))
    
    if (not os.path.isdir(dest_dir)):
        os.makedirs(dest_dir, mode=mode)
    
    items = os.listdir(source_dir)
    if (ignore is not None):
        items = ignore(dest_dir, items)
    
    for item in items:
        s = os.path.join(source_dir, item)
        d = os.path.join(dest_dir, item)
        if (os.path.isdir(s)):
            copy_dir(s, d, mode=mode, ignore=ignore)
        else:
            logger.debug("Copying file: {} -> {}".format(s, d))
            copy2(s, d)

def merge_directories(source_dir, dest_dir, includes=None, results=None):
    # type: (str, str, ConfigType, Dict[str, ConfigType]) -> Tuple[ConfigType, Dict[str, ConfigType]]
    
    ignore_func = partial(_merge_ignore_func, dest_dir) # type: Callable[[str, List[str]], Iterator[str]]
    copy_dir(source_dir, dest_dir, ignore=ignore_func)
    includes, results = merge_configs(source_dir, includes, results)
    return includes, results

def merge_write_configs(dest, includes, result):
    # type: (str, ConfigType, Dict[str, ConfigType]) -> None
    for key, j in result.items():
        j = apply_defaults(j, includes)
        write_yaml(os.path.join(dest, key), j)

def merge(sources, dest):
    # type: (Iterable[str], str) -> None
    
    includes = None
    results = dict()
    
    dest = os.path.abspath(os.path.expanduser(dest))
    for src in sources:
        src = os.path.abspath(os.path.expanduser(src))
        includes, results = merge_directories(src, dest, includes, results)
    
    merge_write_configs(dest, includes, results)

def usage(prefix="YAML-configs merging script", out = None):
    # type: (str, TextIO) -> None
    
    if (out is None):
        out = sys.stdout
    
    name = sys.argv[0]
    lines = \
    [
        prefix,
        "Usage: {} SOURCE_1 [ SOURCE_2 ... ] DEST".format(name),
        '',
        "Merges sources from the given directories",
        "All YAML configs are merged.",
        '',
        "Sources listed on the right overwrite sources from the left",
    ]
    
    for line in lines:
        print(line, file=out)


def main(args = None):
    # type: (List[str]) -> int
    
    if (args is None):
        args = sys.argv[1:]
    
    min_args = 2
    if (len(args) < min_args):
        usage("Not enough arguments, expected at least {}".format(min_args), sys.stderr)
    
    logger.setLevel('DEBUG')
    basicConfig(level='DEBUG')
    merge(args[:-1], args[-1])
    
    return 0

__all__ = \
[
    'main',
    'merge',
    'usage',
]

if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)
