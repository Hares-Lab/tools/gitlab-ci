from setuptools import setup

setup \
(
    name = 'markdown-pretty-lists',
    version = '1.0.0',
    author = 'Peter Zaitcev',
    author_email = 'ussx-hares@yandex.ru',
    description = 'Markdown extension to support short lists definitions (without newline after paragraph)',
    long_description_content_type = 'text/markdown',
    url = 'https://gitlab.com/Hares-Lab/gitlab-ci/scripts/mkdocs/markdown-pretty-lists',
    py_modules = [ 'markdown_pretty_lists' ],
    install_requires = [ 'markdown>=2.5' ],
    classifiers =
    [
        'Operating System :: OS Independent',
        'License :: OSI Approved :: MIT License',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Topic :: Text Processing :: Filters',
        'Topic :: Text Processing :: Markup :: HTML'
    ]
)
