#!/usr/bin/env bash

set -eu

if [[ ! -d "${DOCS_TEMPLATES_DIR}" ]]
then
    install -v -m755 -dD "${DOCS_TEMPLATES_DIR}"
fi

CONFIG_PATH="${DOCS_TEMPLATES_DIR}/$(basename "${DOCS_CONFIG}")"
if [[ ! -f "${CONFIG_PATH}" ]]
then
    DEFAULT_CONFIG=$(python << EOL
import os
from pkg_resources import resource_filename
print(os.path.abspath(resource_filename('pdoc', '${DOCS_CONFIG}')))
EOL
    )
    install -v -m755 -D "${DEFAULT_CONFIG}" "${CONFIG_PATH}"
    # sed -i 's|show_type_annotations = False|show_type_annotations = True|g' "${CONFIG_PATH}"
fi

pdoc --html "${PACKAGE_PYTHON_NAME}" -o="${DOCS_OUT_DIR}/" --template-dir="${DOCS_TEMPLATES_DIR}"

if [[ -d "${DOCS_STATIC_SOURCES_DIR}" ]]
then
    install -v -m755 -dD "${DOCS_OUT_DIR}/${PACKAGE_PYTHON_NAME}/${DOCS_STATIC_DEST_DIR}"
    cp -rv "${DOCS_STATIC_SOURCES_DIR}"/* "${DOCS_OUT_DIR}/${PACKAGE_PYTHON_NAME}/${DOCS_STATIC_DEST_DIR}/"
fi

(grep -rlZ "${DOCS_STATIC_SOURCES_DIR}" "${DOCS_OUT_DIR}/" | xargs -0 sed -i "s|/${DOCS_STATIC_SOURCES_DIR}|/${DOCS_STATIC_DEST_DIR}|g") || :
