from markdown.inlinepatterns import InlineProcessor
from markdown.extensions import Extension
import xml.etree.ElementTree as etree

class StrikethroughInlineProcessor(InlineProcessor):
    def handleMatch(self, m, data):
        el = etree.Element('s')
        classes = [ 'strikethrough' ]
        el.attrib['class'] = ' '.join(classes)
        el.text = m.group(1)
        return el, m.start(0), m.end(0)\

class StrikethroughExtension(Extension):
    def extendMarkdown(self, md):
        pattern_inline = r'~~([^\n]+)~~'  # like ~~text~~
        md.inlinePatterns.register(StrikethroughInlineProcessor(pattern_inline, md), 'strikethrough-inline', 175)
        
        pattern_multiline = r'~~(.*)~~'  # like ~~text~~
        md.inlinePatterns.register(StrikethroughInlineProcessor(pattern_multiline, md), 'strikethrough-multiline', 174)

def makeExtension(**kwargs):
    return StrikethroughExtension(**kwargs)


__all__ = \
[
    'StrikethroughExtension',
    'StrikethroughInlineProcessor',
    'makeExtension',
]
