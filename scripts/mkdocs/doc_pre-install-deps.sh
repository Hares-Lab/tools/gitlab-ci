#!/usr/bin/env bash

set -eu

python -m pip install mkdocs
python -m pip install markdown-checklist

pushd "${SCRIPTS_DIRECTORY}/mkdocs/markdown-inline-diff"
    python -m pip install -e .
popd
