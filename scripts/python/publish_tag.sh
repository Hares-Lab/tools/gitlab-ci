#!/usr/bin/env bash

set -eu

git tag -a "v${ACTUAL_VERSION}" -m "Release ${ACTUAL_VERSION}"
git push origin --tags
