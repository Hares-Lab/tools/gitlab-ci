#!/usr/bin/env bash

set -eu
if [[ ${ACTUAL_VERSION} == *"a" ]] || [[ ${ACTUAL_VERSION} == *"b" ]] || [[ ${ACTUAL_VERSION} == *"rc" ]]
then SUFFIX='*.tar.gz'
else SUFFIX='.tar.gz'
fi

ls "dist/${PACKAGE_NAME//-/_}-${ACTUAL_VERSION//-/}"${SUFFIX} -1 | head -n 1
