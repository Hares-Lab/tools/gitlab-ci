#!/usr/bin/env bash

set -eu

export LC_ALL=en_US.utf-8
export LANG=en_US.utf-8

install -vm755 -dD "${CI_PROJECT_DIR}/public/"

OUTPUT_FILE=$(mktemp)
python -m mkdocs build -v 2>&1 | tee "${OUTPUT_FILE}"

function warnings_or_errors()
{
    cat "${OUTPUT_FILE}" | grep -P 'WARNING|ERROR' | grep -v -P "^WARNING\s+-\s+A relative path to '[^']+' is included in the 'nav' configuration, which is not found in the documentation files\s*$"
}

# Raise on warnings
if warnings_or_errors > /dev/null
then
    echo
    echo "The following Warnings/Errors were found:" >&2
    warnings_or_errors >&2
    exit 1
fi

rm -rv "${OUTPUT_FILE}"
