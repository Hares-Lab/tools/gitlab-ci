#!/usr/bin/env bash

set -eu
set +x

# No need for private token in public repos
CI_API_TOKEN="${CI_API_TOKEN:=}"

INIT_FILE="${PACKAGE_SOURCES_ROOT#"${CI_PROJECT_DIR}/"}/${INIT_SCRIPT_NAME}"
TARGET_BRANCH="${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
BASE_VERSION="$(git show "${REMOTE:=origin}/${TARGET_BRANCH}:${INIT_FILE}" | bash "${SCRIPTS_DIRECTORY}/python/util_get-version.sh")"
CURRENT_VERSION="$(bash "${SCRIPTS_DIRECTORY}/python/util_get-version.sh" < "${INIT_FILE}")"

echo "Base version:    ${BASE_VERSION}"
echo "Current version: ${CURRENT_VERSION}"

IFS='.-/' read -ra CURRENT_VERSION_SPLIT <<< "${CURRENT_VERSION}"
IFS='.-/' read -ra BASE_VERSION_SPLIT <<< "${BASE_VERSION}"
for i in $(seq 0 $((${#CURRENT_VERSION_SPLIT[@]} - 1)))
do
    if (( i > ((${#BASE_VERSION_SPLIT[@]} - 1)) ))
    then
        echo "Version upgraded with a new digit"
        exit 0
    elif grep -P '[^0-9]' <<< "${CURRENT_VERSION_SPLIT[$i]}${BASE_VERSION_SPLIT[$i]}" > /dev/null
    then
        echo "Non-digit symbols detected"
        echo "Counting as upgraded"
        exit 0
    elif (( CURRENT_VERSION_SPLIT[i] > BASE_VERSION_SPLIT[i] ))
    then
        echo "Version upgraded"
        exit 0
    elif (( CURRENT_VERSION_SPLIT[i] == BASE_VERSION_SPLIT[i] ))
    then continue
    else
        echo "Version downgraded" >&2
        exit 1
    fi
done

echo "Version not upgraded" >&2
exit 1
