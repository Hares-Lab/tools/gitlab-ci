#!/usr/bin/env bash

set -eu

if jq --version &> /dev/null
then
    echo "jq is already installed"
    exit 0
fi

JQ_NAME="${JQ_NAME:="jq-${JQ_VERSION:=1.5}"}"

pushd /tmp
    wget "https://github.com/stedolan/jq/releases/download/${JQ_NAME}/${JQ_NAME}.tar.gz"
    tar -xzf "${JQ_NAME}.tar.gz"
    cd "${JQ_NAME}/"
    ./configure
    make
    make install
popd

jq --version
