# GitLab CI Utilities Repository
This repository provides utilities for other projects.
Currently supporting Python3 and MkDocs sites.

## Python Projects
### Stages
1. Setup
2. Build
   - Build (also copies resources to the distribution)
   - Create wheel
   - Install package
3. Test
   - Test
   - Create documentation
   - *(Merge Requests only)* Check version is upgraded
   - *(Merge Requests, Tags, master/main branches, and manual launches only)* Python compatibility tests
4. Publish
   - *(master/main branch only)* Publish docs via GitLab Pages
   - *(master/main branch only)* Create and publish git tag and GitLab release
   - *(Tags only)* Check version is not released
   - *(Tags only)* Publish PyPi wheel release

### GitLab CI config
Add the following `.gitlab-ci.yml` file to your Python repository.
Please, note that the value for `PACKAGE_PYTHON_NAME` MUST be filled.
```yaml
include:
  - project: 'hares-lab/tools/gitlab-ci'
    file: '/.gitlab-ci.python.yml'

variables: 
  # Project-dependent
  PACKAGE_PYTHON_NAME: 'YOUR_PYTHON_MODULE_NAME_WITH_UNDESCORES_HERE'

stages:
  - setup
  - build
  - test
  - publish

# Environment preparation
setup:
  extends: .setup

# Build
build:
  extends: .build

# Test
test:
  extends: .test

test:multi-python:
  extends: .test:multi-python

doc:
  extends: .doc

check-version-upgrade:
  extends: .check-version-upgrade

# Publish
create-tag:
  extends: .create-tag

publish:
  extends: .publish

pages:
  extends: .pages
```

### Environment Variables Used
The following protected, masked variables should be added
to the environment for proper publication step:
 - ~~`CI_PRIVATE_TOKEN`:~~  
 ~~A colon-concatenated GitLab User and GitLab Private Token.~~
 ~~Used for tag publication.~~
 - `PYPI_USER`:  
 PyPi username.
 In case of PyPI Personal Access Tokens, use `__token__`.
 Used for PyPI wheel publication.
 - `PYPI_PASSWORD`:  
 PyPi user password, or PyPI Personal Access Token.
 Used for PyPI wheel publication.
 - `PYPI_REPOSITORY`:  
 Optional. PyPI repository URL used for publication.
 Used for PyPI wheel publication.
 - `PIP_CONFIG`:  
 Optional. Plain text **non-file** variable containing *pip* config.
 Used for package downloading.

### TODOs
 - [x] Support non-standard PyPI repositories
 - [ ] Support docs versioning
 - [x] Add Python version-compatibility tests
 - [ ] Support code coverage
 - [ ] Support unit test reports
 - [ ] Use file-type variables


## MkDocs Sites
### GitLab CI config
```yaml
include:
  - project: 'hares-lab/gitlab-ci'
    file: '/.gitlab-ci.mkdocs.yml'
```

### Example MkDocs config (Optional)
From the [Runeterra RPG](https://gitlab.com/triplet-rpg/runeterra-rpg) repository.

```yaml
use_directory_urls: false
theme:
  name: mkdocs
  icon:
    logo:
      img/runeterra-1.jpg
  favicon: img/favicon.ico

plugins:
  - '#default'
  - search:
      lang:
        - en
        - ru

copyright: Copyright &copy; 2020-2021 Peter Zaitcev and Gregory Koroteev
google_analytics: [ "UA-186112545-1", "triplet-rpg.gitlab.io/runeterra-rpg/" ]

# nav:
# Some Nav action here
```
