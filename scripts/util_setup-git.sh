#!/usr/bin/env bash

set -eu

BRANCH="${BRANCH:=$(git branch | grep \* | tr -d \* | tr -d ' ')}"
REMOTE="${REMOTE:=origin}"

git config user.name "$(git log -1 --pretty=format:'%an')"
git config user.email "$(git log -1 --pretty=format:'%ae')"
# git remote set-url ${REMOTE} "$(echo "${CI_PROJECT_URL}" | sed -r 's|^\w+://([a-zA-Z0-9.-]+)/|git@\1:|g').git"
git fetch --all
git reset --hard ${REMOTE}/${BRANCH}
git pull ${REMOTE} ${BRANCH}
git branch -D ${BRANCH} || :
git checkout ${BRANCH}
git status
