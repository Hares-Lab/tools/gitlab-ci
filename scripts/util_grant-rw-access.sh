#!/usr/bin/env bash

set -eu

REMOTE="${REMOTE:=origin}"
GITLAB_CI_USER="${GITLAB_CI_USER:=gitlab-ci-token}"

NEW_URL="${CI_REPOSITORY_URL/"${GITLAB_CI_USER}:${CI_JOB_TOKEN}"/"${CI_PRIVATE_TOKEN}"}"
git remote set-url "${REMOTE}" "${NEW_URL}"
