#!/usr/bin/env bash

set -eu

function join_by { local IFS="$1"; shift; echo "$*"; }

FEATURES=()
DEV_INSTALL=false
FLAGS=
for ARG in "$@"
do
    case "${ARG}" in
        --dev)
            DEV_INSTALL=true
            ;;
        
        -*)
            echo Unsuppored option: "${ARG}" >&2
            exit 1
            ;;
        
        *)
            FEATURES+=("${ARG}")
            ;;
    esac
done

FEATURE_FLAGS="[$(join_by , ${FEATURES:=})]"
if [[ ${FEATURE_FLAGS} == '[]' ]]
then FEATURE_FLAGS=
fi

if [[ "${DEV_INSTALL}" == true ]]
then TARGET="-e ."
else TARGET="$(bash ${BASH_OPTIONS} "${SCRIPTS_DIRECTORY}/python/util_find-wheel.sh")"
fi

pip install ${TARGET}${FEATURE_FLAGS}
