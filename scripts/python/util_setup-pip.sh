#!/usr/bin/env bash

set -eu

install -D -vm 600 /dev/null "${PIP_CONFIG_FILE}"
echo ${PIP_CONFIG:-} > "${PIP_CONFIG_FILE}"
