#!/usr/bin/env bash

set -eu

ls -al dist/
twine upload "$(bash ${BASH_OPTIONS} "${SCRIPTS_DIRECTORY}/python/util_find-wheel.sh")"
twine upload "$(bash ${BASH_OPTIONS} "${SCRIPTS_DIRECTORY}/python/util_find-sdist.sh")"
