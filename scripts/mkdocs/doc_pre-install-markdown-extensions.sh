#!/usr/bin/env bash

set -eu

pushd "${SCRIPTS_DIRECTORY}/mkdocs/markdown-inline-diff"
    python -m pip install -e .
    install -dD -vm 755 "${DOCS_DIR}/${CSS_REL_PATH}"
    install -vm 644 'inline-diff.css' "${DOCS_DIR}/${CSS_REL_PATH}/"
popd

pushd "${SCRIPTS_DIRECTORY}/mkdocs/markdown-pretty-lists"
    python -m pip install -e .
popd

pushd "${SCRIPTS_DIRECTORY}/mkdocs/markdown-strikethrough"
    python -m pip install -e .
popd

pushd "${SCRIPTS_DIRECTORY}/mkdocs/markdown-symbols-replacement"
    python -m pip install -e .
popd

pushd "${SCRIPTS_DIRECTORY}/mkdocs/mkdocs-extensions"
    python -m pip install -e .
popd

pushd "${SCRIPTS_DIRECTORY}/mkdocs/slugify-smart"
    python -m pip install -e .
popd
