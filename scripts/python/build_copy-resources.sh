#!/usr/bin/env bash

set -eu

if [[ -d "${RESOURCES_DIRECTORY}" ]]
then
    echo "Copying resources directory into the package"
    TARGET_DIR="${PACKAGE_SOURCES_ROOT}/${RESOURCES_DIRECTORY_NAME}"
    install -v -m 755 -dD "${TARGET_DIR}"
    cp -rv "${RESOURCES_DIRECTORY}/"* "${TARGET_DIR}/"
else
    echo "Resources directory not found, skipping..."
fi
