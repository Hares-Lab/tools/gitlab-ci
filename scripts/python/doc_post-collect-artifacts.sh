#!/usr/bin/env bash

set -eu

mkdir -pv public/
mv -v "${DOCS_OUT_DIR}/${PACKAGE_PYTHON_NAME}"/* public/
