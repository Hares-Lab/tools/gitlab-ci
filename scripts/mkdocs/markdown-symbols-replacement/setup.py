from setuptools import setup

setup \
(
    name = 'markdown-symbols-replacement',
    version = '1.0.0',
    author = 'Peter Zaitcev',
    author_email = 'ussx-hares@yandex.ru',
    description = 'Markdown extension to support simple pattern replacements',
    long_description_content_type = 'text/markdown',
    url = 'https://gitlab.com/Hares-Lab/gitlab-ci/scripts/mkdocs/markdown-symbols-replacement',
    py_modules = [ 'markdown_symbols_replacement' ],
    install_requires = [ 'markdown>=2.0' ],
    classifiers =
    [
        'Operating System :: OS Independent',
        'License :: OSI Approved :: BSD License',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Topic :: Text Processing :: Filters',
        'Topic :: Text Processing :: Markup :: HTML'
    ]
)
