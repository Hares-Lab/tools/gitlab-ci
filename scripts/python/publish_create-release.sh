#!/usr/bin/env bash

set -eu

# CHANGELOG=$(cat './README.md')
CHANGELOG=$(cat << EOF
# Release ${ACTUAL_VERSION}
### Changelog: **TBD**
EOF
)
DATA=$(echo '{}' \
    | jq --arg value "Release ${ACTUAL_VERSION}" '.name = $value' \
    | jq --arg value "v${ACTUAL_VERSION}" '.tag_name = $value' \
    | jq --arg value "${CI_COMMIT_SHORT_SHA}" '.ref = $value' \
    | jq --arg value "${CHANGELOG}" '.description = $value' \
)
curl --verbose --fail \
    -X POST "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases" \
    -H "JOB-TOKEN: ${CI_JOB_TOKEN}" \
    -H "Content-Type: application/json" \
    --data "${DATA}" \
