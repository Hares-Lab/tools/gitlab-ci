#!/usr/bin/env bash

set -eu

if [[ -f "${MKDOCS_CONF_NAME}" ]]
then
    echo "File '${MKDOCS_CONF_NAME}'' already exists"
else
    TEMP_DIR=$(mktemp -d -t mkdocs-XXXXXX.conf.d)
    EDIT_URI=''
    if [[ ! -z "${CI_COMMIT_REF_NAME}" ]] && [[ "${CI_COMMIT_REF_NAME}" != "${CI_DEFAULT_BRANCH}" ]]
    then EDIT_URI="edit_uri: 'edit/${CI_COMMIT_REF_NAME}/docs/'"
    fi
    cat << EOF > "${TEMP_DIR}/mkdocs.default.yml"
site_name: ${CI_PROJECT_TITLE}
site_author: ${GITLAB_USER_EMAIL}
site_url: ${CI_PAGES_URL}
repo_url: ${CI_PROJECT_URL}
repo_name: GitLab
${EDIT_URI}

docs_dir: ${DOCS_REL_PATH}
site_dir: public
dev_addr: "localhost:8080"

plugins:
  - root
  - wildcard-css
  - readme:
      docs_dir: '${DOCS_REL_PATH}'
      source: '${README_NAME}'

markdown_extensions:
  - toc:
      slugify: !!python/name:slugify_smart.slugify_smart
  - markdown_symbols_replacement:
      replacements:
        '<<': '«'
        '>>': '»'
        '--': '—'
        '->': '→'
        '=>': '⇒'
        '<-': '←'
        '<=': '⇐'

  - markdown_inline_diff
  - markdown_pretty_lists
  - markdown_strikethrough
  - markdown_checklist.extension

EOF

    if [[ -f "${MKDOCS_NAV_CONF_NAME}" ]]
    then cp -v "${MKDOCS_NAV_CONF_NAME}" "${TEMP_DIR}/$(basename "${MKDOCS_CONF_NAME}")"
    else touch "${TEMP_DIR}/$(basename "${MKDOCS_CONF_NAME}")"
    fi

    python "${SCRIPTS_DIRECTORY}/mkdocs/doc_merge-mkdocs-conf.py" "${TEMP_DIR}" $(dirname "${MKDOCS_CONF_NAME}")
    rm -rvf "${TEMP_DIR}"
fi

echo "Config dump:"
cat "${MKDOCS_CONF_NAME}"
