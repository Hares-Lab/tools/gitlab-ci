import re
import html
from typing import *
from typing.re import *

from markdown.inlinepatterns import InlineProcessor
from markdown.extensions import Extension

class SymbolsReplacementInlineProcessor(InlineProcessor):
    repl: str
    
    def __init__(self, pattern: str, repl: str, md=None):
        super().__init__(pattern, md)
        self.repl = repl
    
    def handleMatch(self, m: Match, data):
        # print(f"Replacing {m.group(0)!r} from {m.string!r} with {self.repl!r}")
        return self.repl, m.start(0), m.end(0)

DEFAULT_REPLACEMENTS = \
{
  '<<': '«',
  '>>': '»',
  '--': '—',
}

class SymbolsReplacementExtension(Extension):
    config = { 'replacements': [ dict, "A map of replacements. Each object is escaped and thus regular expression could not be used" ] }
    
    def extendMarkdown(self, md):
        replacements: Dict[str, str] = self.getConfig('replacements', default=DEFAULT_REPLACEMENTS)
        # print(f"Replacements are: {replacements}")
        for i, (pat, repl) in enumerate(replacements.items()):
            # print(f"Registering replacement: {re.escape(pat)!r} ({pat!r}) => {repl!r}")
            md.inlinePatterns.register(SymbolsReplacementInlineProcessor(pat, repl, md), f'symbols-repl-{i}', 175)

def makeExtension(**kwargs):
    return SymbolsReplacementExtension(**kwargs)


__all__ = \
[
    'SymbolsReplacementExtension',
    'SymbolsReplacementInlineProcessor',
    'makeExtension',
]
